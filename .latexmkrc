# interpret missing, unlocked git-annex'ed files correctly as missing
push @file_not_found, '!pdfTeX error:.*?\(file ([^)]+)\):.*?reading \S+ image failed';
$make = "git annex get"; # or "datalad get", works the same
$use_make_for_missing_files = 1;
$go_mode = 1;
$preview_continuous_mode = 1;
$force_make_file_regex = qr/.*\.pdf/;
