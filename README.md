# latexmk biber endless loop demo

This is a minimal working example of latexmk getting stuck in an endless loop in the following situation:

- `\includegraphics`ed corrupt PDF that is not actually a PDF stops pdflatex from proceeding. (With an extra regex in `.latexmkrc`, `latexmk` correctly sees that PDF as *missing*).
- Thus the `biblatex` package can't finish the XML in the `.bcf` file
- `latexmk` proceeds to execute `biber` on said broken `.bcf`
- `biber` fails to parse the broken XML (and deletes the `.bbl`)
- latexmk is now stuck in an endless loop, trying to run `biber` again and again
- **This could be solved by having latexmk fetch missing files first, before attempting any bibliography stuff. I don't know how to teach it to do that.**

## Reproduce

You can also just leave out the `nix develop --command` prefix below if you provide the environment yourself.

You can replace the `git annex drop` with `echo bla > image.pdf` and the `git annex get` with `curl -sL 'https://gitlab.com/nobodyinperson/humidity-diagram/-/raw/master/humidity-diagram.pdf?inline=false' > image.pdf` to simulate the same effect.

```bash
nix develop --command git annex drop # make sure the PDF is not there
nix develop --command latexmk -gg -use-make -pvc # endless loop

nix develop --command git annex get # get the PDF
nix develop --command latexmk -gg -use-make -pvc # PDF is opened
```
